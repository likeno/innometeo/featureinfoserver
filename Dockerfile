FROM python:3.7

RUN apt update && \
    apt install --yes libeccodes0

RUN adduser --uid 1001 --gecos '' --disabled-password likeno

WORKDIR /home/likeno/featureinfoserver

COPY --chown=likeno requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY --chown=likeno . .

RUN chown -R likeno:likeno .

RUN pip install --editable .

USER likeno

EXPOSE 8000

ENTRYPOINT [\
    "gunicorn", \
    "--reload", \
    "--bind=0.0.0.0:8000", \
    "--workers=4", \
    "--access-logfile=-", \
    "--error-logfile=-", \
    "featureinfoserver.application:api"\
]

from setuptools import find_packages
from setuptools import setup


setup(
    name='featureinfoserver',
    version='0.4.7',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'falcon',
        'gunicorn',
    ]
)

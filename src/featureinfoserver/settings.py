import os
from shapely import wkt


def get_value_from_env(name, default=None, cast=None):
    raw_value = os.getenv(name)
    if raw_value is None:
        if default is None:
            raise RuntimeError(f"Please provide the {name!r} variable")
        else:
            result = default
    else:
        result = cast(raw_value) if cast is not None else raw_value
    return result


def get_boolean_from_env(name, default=None):
    raw_value = os.getenv(name)
    if raw_value is None:
        if default is None:
            raise RuntimeError(f"Please provide the {name!r} variable")
        else:
            result = raw_value
    else:
        truthy = (
            "1",
            "yes",
            "true",
        )
        result = True if raw_value.lower() in truthy else False
    return result


DEBUG = get_boolean_from_env("DEBUG", default=False)

DATA_ROOT = get_value_from_env("DATA_ROOT", default="/home/likeno/data")

LEVEL_NAME = "isobaricInhPa"
SURFACE_LEVEL = "surface"

AROME = {
    "areas": {
        "PT2": wkt.loads(
            "POLYGON(("
            "-1424889 4004067, "
            "-134255 4004067, "
            "-134255 5559971, "
            "-1424889 5559971, "
            "-1424889 4004067"
            "))"
        ),
        "AZO": wkt.loads(
            "POLYGON(("
            "-3683561 4237912, "
            "-2678369 4237912, "
            "-2678369 4963754, "
            "-3683561 4963754, "
            "-3683561 4237912"
            "))"
        ),
        "MAD": wkt.loads(
            "POLYGON(("
            "-2113957 3609767, "
            "-1647146 3609767, "
            "-1647146 4105604, "
            "-2113957 4105604, "
            "-2113957 3609767"
            "))"
        )
    },
    "pattern": (
        "{{ data_root }}/output/innometeo_arome/"
        "{{ reference_time|format('%Y/%m/%d') }}/"
        "AROME_OPER_001_FC_SP_{{ area }}_025_{{ pattern }}_"
        "{{ reference_time|format('%Y%m%d') }}{{ run|format('02d') }}.nc"
    ),
    "pressure_levels": [950, 925, 900, 850, 800, 700, 500, 300],
    "layers": {
        "wind_intensity": [
            {"pattern": "wind-intensity-10m", "name": "intensity"},
            {"pattern": "wind-intensity", "name": "intensity", "levels": True},
        ],
        "wind_direction": [
            {"pattern": "wind-direction-10m", "name": "direction"},
            {"pattern": "wind-direction", "name": "direction", "levels": True},
        ],
        'temperature': [
            {'pattern': '2t_merc', 'name': 't2m'},
            {'pattern': 't_merc', 'name': 't', 'levels': True}
        ],
        'dewpoint': [
            {'pattern': '2d_merc', 'name': 'd2m'}
        ],
        'high_cloud_cover': [
            {'pattern': 'hcc_merc', 'name': 'hcc'}
        ],
        'medium_cloud_cover': [
            {'pattern': 'mcc_merc', 'name': 'mcc'}
        ],
        'low_cloud_cover': [
            {'pattern': 'lcc_merc', 'name': 'lcc'}
        ],
        'total_cloud_cover': [
            {'pattern': 'tcc_merc', 'name': 'tcc'}
        ],
        'pressure': [
            {'pattern': 'msl_merc', 'name': 'msl'}
        ],
        'precipitation': [
            {'pattern': 'tp_merc', 'name': 'tp'}
        ],
        'vertical_velocity': [
            {'pattern': 'w_merc', 'name': 'w', 'levels': True},
        ],
        'geopotential': [
            {'pattern': 'z_merc', 'name': 'z', 'levels': True},
        ],
        'relative_humidity': [
            {'pattern': 'r_2m-merc', 'name': 'r'},
            {'pattern': 'r_merc', 'name': 'r', 'levels': True},
        ],
    }
}

ECMWF = {
    "areas": {
        "ATP": wkt.loads(
            "POLYGON(("
            "-4124387 3228137, "
            "3876 3228137, "
            "3876 5870227, "
            "-4124387 5870227, "
            "-4124387 3228137"
            "))"
        ),
    },
    "pattern": (
        "{{ data_root }}/output/innometeo_ecmwf/"
        "{{ reference_time|format('%Y/%m/%d') }}/"
        "ECMWF_OPER_001_FC_SP_{{ area }}_090_{{ pattern }}_"
        "{{ reference_time|format('%Y%m%d') }}{{ run|format('02d') }}.nc"
    ),
    "pressure_levels": [925, 900, 850, 800, 500, 300],
    "layers": {
        "dewpoint": [
            {"pattern": "2d_merc", "name": "d2m"}
        ],
        "temperature": [
            {"pattern": "2t_merc", "name": "t2m"},
            {"pattern": "t_merc", "name": "t", "levels": True}
        ],
        "relative_humidity": [
            {"pattern": "r_2m-merc", "name": "r"},
            {"pattern": "r_merc", "name": "r", "levels": True}
        ],
        "pressure": [
            {'pattern': "msl_merc", "name": "msl"}
        ],
        "wind_intensity": [
            {"pattern": "wind-intensity-10m_merc", "name": "intensity"},
            {"pattern": "wind-intensity_merc", "name": "intensity", "levels": True},
        ],
        "wind_direction": [
            {"pattern": "wind-direction-10m_merc", "name": "direction"},
            {"pattern": "wind-direction_merc", "name": "direction", "levels": True},
        ],
        "geopotential": [
            {"pattern": "z_0m-merc", "name": "z"},
            {"pattern": "z_merc", "name": "z", "levels": True}
        ],
        "cape": [
            {"pattern": "cape_merc", "name": "cape"},
        ],
        "capes": [
            {"pattern": "capes_merc", "name": "capes"},
        ],
        "precipitation": [
            {"pattern": "pcp_merc", "name": "p83.128"},
        ]

    }
}

ECMWF_ML = {
    "areas": {
        "ATP": wkt.loads(
            "POLYGON(("
            "-4124387 3228137, "
            "3876 3228137, "
            "3876 5870227, "
            "-4124387 5870227, "
            "-4124387 3228137"
            "))"
        ),
    },
    "pattern": (
        "{{ data_root }}/output/innometeo_ecmwf/"
        "{{ reference_time|format('%Y/%m/%d') }}/"
        "ECMWF_OPER_001_FC_ML_{{ area }}_090_{{ pattern }}_"
        "{{ reference_time|format('%Y%m%d') }}{{ run|format('02d') }}.grib"
    ),
    "layers": {
        "lnsp": [
            {"pattern": "lnsp", "name": "lnsp"}
        ],
        "wind_u": [
            {"pattern": "u", "name": "u"}
        ],
        "wind_v": [
            {"pattern": "v", "name": "v"}
        ],
        "temperature": [
            {"pattern": "t", "name": "t"}
        ],
        "specific_humidity": [
            {"pattern": "q", "name": "q"}
        ]
    }
}

LSASAF = {
    "areas": {
        "lsasaf_pt": wkt.loads(
            "POLYGON(("
            "-1424889 4029057, "
            "-133537 4029057, "
            "-133537 5590091, "
            "-1424889 5590091, "
            "-1424889 4029057"
            "))"
        ),
    },
    "pattern": (
        "{{ data_root }}/output/innometeo_lsasaf/{{ dir_name }}/"
        "{{ reference_time|format('%Y/%m/%d') }}/"
        "LSASAF_MSG_{{ pattern }}_Iberia-merc_{{ reference_time|format('%Y%m%d') }}1200.nc"
    ),
    "layers": {
        # defaults:
        # - pattern: uppercase of the dict key (uppercase of layer name)
        # - variable_name: uppercase of the dict key (uppercase of layer name)
        "bui": {"dir_name": "frm"},
        "dc": {"dir_name": "frm"},
        "dmc": {"dir_name": "frm"},
        "ffmc": {"dir_name": "frm"},
        "fwi": {"dir_name": "frm"},
        "isi": {"dir_name": "frm"},
        "p2000": {"dir_name": "frm"},
        "p2000a": {
            "dir_name": "frm",
            "pattern": "P2000a",
            "variable_name": "P2000a"
        },
        "risk": {"dir_name": "frm", "variable_name": "Risk"},
    }
}

IPMA = {
    "areas": {
        "ipma_pt_continent": wkt.loads(
            "POLYGON(("
            "-1066994 4431177, "
            "-686003 4431177, "
            "-686003 5185360, "
            "-1066994 5185360, "
            "-1066994 4431177"
            "))"
        ),
    },
    "pattern": (
        "{{ data_root }}/output/innometeo_ipma/"
        "{{ reference_time|format('%Y/%m/%d') }}/"
        "IPMA_OPER_FRM_H24_H168_PT2_010_{{ reference_time|format('%Y%m%d') }}12.nc"
    ),
    "layers": {
        "bui": None,
        "dc": None,
        "dmc": None,
        "ffmc": None,
        "fwi": None,
        "isi": None,
        "p2000": None,
        "p2000a": {
            "dir_name": "frm",
            "pattern": "P2000a",
            "variable_name": "P2000a"
        },
        "risk": {
            "variable_name": "Risk"
        },
    }
}

SKEWT_SOURCE_DATA = {
    'levels': '/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_levels_merc-2019013000.nc',
    'surface-wind': '/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_surface-wind_merc-2019013000.nc',
    'surface': '/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_surface_merc-2019013000.nc',
    'indices': '/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_indices_merc-2019013000.nc',
}

SKEWT_SOURCE_DATA_PATTERNS = {
    "levels": "/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_levels_merc-{{ reference_time|format(%Y%m%d) }}{{ run|format('02d') }}.nc",
    "surface-wind": "/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_surface-wind_merc-{{ reference_time|format(%Y%m%d) }}{{ run|format('02d') }}.nc",
    "surface": "/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_surface_merc-{{ reference_time|format(%Y%m%d) }}{{ run|format('02d') }}.nc",
    "indices": "/output/ecmwf_levels/ECMWF_OPER_001_FM_ML_PTG_090_indices_merc-{{ reference_time|format(%Y%m%d) }}{{ run|format('02d') }}.nc",
}

OBSERVATIONS = {
    "datasets": {
        "pressure": {
            "native_name": "Pr_nm",
            "units": "hPa",
            "extent": [],
        },
        "temperature-minimum": {
            "native_name": "Tar_min",
            "units": "ºC",
            "extent": [],
        },
        "temperature-mean": {
            "native_name": "Tar_med",
            "units": "ºC",
            "extent": [],
        },
        "temperature-maximum": {
            "native_name": "Tar_max",
            "units": "ºC",
            "extent": [],
        },
        "relative-humidity-minimum": {
            "native_name": "HR_min",
            "units": "%",
            "extent": [],
        },
        "relative-humidity-mean": {
            "native_name": "HR_med",
            "units": "%",
            "extent": [],
        },
        "relative-humidity-maximum": {
            "native_name": "HR_max",
            "units": "%",
            "extent": [],
        },
        "wind-direction-mean": {
            "native_name": "dd_med",
            "units": "º",
            "extent": [],
        },
        "wind-intensity-mean": {
            "native_name": "ff_med",
            "units": "m/s",
            "extent": [],
        },
        "wind-direction-maximum": {
            "native_name": "dd_ff_max",
            "units": "º",
            "extent": [],
        },
        "wind-maximum-intensity": {
            "native_name": "ff_max_inst",
            "units": "m/s",
            "extent": [],
        },
        "precipitation": {
            "native_name": "RRR_qt",
            "units": "mm",
            "extent": [],
        },
        "precipitation-maximum": {
            "native_name": "RRR_intmax",
            "units": "mm/h",
            "extent": [],
        },
    },
    "pattern": (
        "{{ data_root }}/observations_data/stations_obs_10m_last_6h.geojson"
    )
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "[{asctime}] [{levelname}] [{name}] {message}",
            "style": "{",
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
    },
    "loggers": {
        "featureinfoserver": {
            "level": "DEBUG" if DEBUG else "WARNING",
            "handlers": ["console"],
            "propagate": True,
        }
    },
}

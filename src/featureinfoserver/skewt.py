import math
from .ecmwf import ECMWF_MODEL_LEVELS


def convert_longitude(value):
    return 360.0 + value


def get_pressure_from_log_pressure(log_pressure):
    """
    Returns e raised to the log_pressure
    """
    return math.e ** log_pressure


def get_level_pressure(level, surface_pressure):
    """
    Returns the true pressure in Pa for a level, expecting the level code
    and the surface_pressure in Pa.
    """
    level_definition = ECMWF_MODEL_LEVELS['levels'][level]
    return level_definition['a'] + level_definition['b'] * surface_pressure


def get_level_geometric_altitude(level):
    return ECMWF_MODEL_LEVELS['levels'][level]['geometric_altitude']


def specific_humidity_to_dewpoint_temperature(humidity, pressure):
    caux = (5423 / 273) + math.log(0.622 * 6.113)
    return 5423 / (caux - math.log((pressure / 100) * humidity))

"""Utilities for dealing with feature info for IPMA and LSASAF files"""

import abc
import datetime as dt
import logging
import typing
from pathlib import Path

import numpy as np
import pandas as pd
import xarray as xr
from jinja2 import Template
from shapely import geometry

from . import settings

logger = logging.getLogger(__name__)


class BaseSampler(abc.ABC):
    HOUR_STEPS = (
        24,
        48,
        72,
        96,
        120,
    )

    def find_best_prediction(
            self,
            relevant_instant: dt.datetime,
            path_template: Template,
            path_context: typing.Dict
    ) -> typing.Optional[typing.Tuple[Path, int]]:
        """Find the most current prediction for the input time"""
        for step in self.HOUR_STEPS:
            step_reference_time = relevant_instant - dt.timedelta(hours=step)
            context = path_context.copy()
            context.update(reference_time=step_reference_time)
            path = Path(path_template.render(**context))
            logger.debug(f"looking for path: {path!r}...")
            if path.is_file():
                result = (path, step)
                break
        else:
            result = None
        return result

    @abc.abstractmethod
    def sample_dataset(
            self,
            dataset: xr.Dataset,
            variable_name: str,
            point: geometry.Point,
            time: dt.datetime,
            step: int,
    ) -> typing.Optional[float]:
        raise NotImplementedError


class LsasafSampler(BaseSampler):

    def sample_dataset(
            self,
            dataset: xr.Dataset,
            variable_name: str,
            point: geometry.Point,
            time: dt.datetime,
            step: int,
    ) -> typing.Optional[float]:
        try:
            temporal_filter = dataset.data_vars[variable_name].sel(
                time=time,
                step=pd.Timedelta(hours=step)
            )
        except KeyError:
            result = None
        else:
            sampled = temporal_filter.sel(
                x=point.x,
                y=point.y,
                time=time,
                step=pd.Timedelta(hours=step),
                method="nearest"
            )
            logger.debug(f"sampled: {sampled}")
            result = float(sampled.values)
        return result if not np.isnan(result) else None


class IpmaSampler(BaseSampler):
    # - IPMA products do not have a `step` dimension, they have only a `time`
    #   dimension, which is a datetime
    # - Additionally, the base unit of the `time` dimension is the date of the
    #   first step, not the file's reference time

    def sample_dataset(
            self,
            dataset: xr.Dataset,
            variable_name: str,
            point: geometry.Point,
            time: dt.datetime,
            step: int,
    ) -> float:
        try:
            temporal_filter = dataset.data_vars[variable_name].sel(time=time)
        except KeyError:
            result = None
        else:
            sampled = temporal_filter.sel(
                x=point.x,
                y=point.y,
                method="nearest"
            )
            logger.debug(f"sampled: {sampled}")
            result = float(sampled.values)
        return result if not np.isnan(result) else None


def get_sampler(provider: str):
    sampler_class = {
        "ipma": IpmaSampler,
        "lsasaf": LsasafSampler,
    }[provider]
    return sampler_class()


def get_relevant_times(
        base_time: dt.datetime
) -> typing.List[dt.datetime]:
    """Build a list of relevant times for IPMA and LSASAF products.

    Relevant time period spans 7 days in the past and 3 days in the future.

    """

    start = base_time - dt.timedelta(days=7)
    end = base_time + dt.timedelta(days=3)
    result = []
    current = start
    while current <= end:
        result.append(current)
        current += dt.timedelta(days=1)
    return result


def retrieve_layer_info(
        layer_name: str,
        provider: str,
        point: geometry.Point,
        layer_parameters: typing.Dict,
        path_template: Template,
        base_time: dt.datetime,
) -> typing.List[typing.Dict]:
    relevant_times = get_relevant_times(base_time)
    logger.debug(f"relevant_times: {relevant_times}")
    result = []
    sampler = get_sampler(provider)
    for target_time in relevant_times:
        logger.debug(f"Handling time {target_time!r}")
        best_prediction = sampler.find_best_prediction(
            relevant_instant=target_time,
            path_template=path_template,
            path_context={
                "data_root": settings.DATA_ROOT,
                "dir_name": layer_parameters.get(
                    "dir_name",
                    layer_name.upper()
                ),
                "pattern": layer_parameters.get("pattern", layer_name.upper()),
            }
        )
        logger.debug(f"best_prediction: {best_prediction}")
        if best_prediction is None:
            logger.debug(
                f"Could not find a suitable file for time {target_time!r}")
            continue
        path, step = best_prediction
        with xr.open_dataset(path) as ds:
            value = sampler.sample_dataset(
                ds,
                layer_parameters.get("variable_name", layer_name.upper()),
                point,
                target_time,
                step,
            )
            logger.info(f"value: {value}")
            if value is not None:
                result.append({
                    'value': value,
                    'time': target_time.isoformat(),
                    'level': settings.SURFACE_LEVEL,
                })
    return result

"""Utilities for dealing with feature info form station observation files"""

import datetime as dt
import logging
import typing

import dateutil.parser
import geojson
from pathlib import Path
from shapely import geometry

from . import settings

logger = logging.getLogger(__name__)
ENCODING = "utf-8"


def extract_observation_data(
        observations_path: Path,
        dataset: str,
        instant: typing.Optional[dt.datetime] = None,
        bbox: typing.Optional[geometry.Polygon] = None,
) -> typing.List[geojson.Feature]:
    result = []
    dataset_config = settings.OBSERVATIONS["datasets"][dataset]
    to_keep = [
        "time",
        "station_id",
        "name_st",
        "height",
    ]
    with observations_path.open(encoding=ENCODING) as fh:
        features = geojson.load(fh)["features"]
        # sort features according to their 'time' in descending order - this is
        # done to avoid having to iterate through the whole collection
        features.sort(
            key=lambda feature: feature["properties"]["time"],
            reverse=True
        )
        for feature in features:
            to_append = False
            if instant is None:
                to_append = True
            else:
                feature_time = dateutil.parser.parse(
                    feature["properties"]["time"])
                if feature_time < instant:  # `features` is sorted in descending order
                    break
                elif feature_time == instant:  # this feature's time matches the input 'instant'
                    if bbox is None:
                        to_append = True
                    else:
                        feature_point = geometry.asShape(feature["geometry"])
                        if bbox.intersects(feature_point):
                            to_append = True
            if to_append:
                new_properties = {
                    prop: feature["properties"][prop] for prop in to_keep}
                new_properties.update({
                    dataset: feature[
                        "properties"][dataset_config["native_name"]],
                    "units": dataset_config["units"],
                })
                feature["properties"] = new_properties
                result.append(feature)
    return result


def extract_station_data(
        observations_path: Path,
        station_id: int,
        datasets: typing.Optional[typing.List[str]] = None,
) -> typing.Tuple[typing.Dict[str, typing.List], typing.Dict]:
    to_sample = {}
    for name, dataset_config in settings.OBSERVATIONS["datasets"].items():
        if datasets is None or name in datasets:
            to_sample[name] = dataset_config["native_name"]
    data_series = {ds: [] for ds in to_sample.keys()}
    geometry = None
    with observations_path.open(encoding=ENCODING) as fh:
        features = geojson.load(fh)["features"]
        # sort features according to their 'station_id' and their `time` -
        # this is done to avoid having to iterate through the whole collection
        features.sort(
            key=lambda feat: (
                feat["properties"]["station_id"],
                feat["properties"]["time"]
            )
        )
        found_station_id = False
        for feature in features:
            if feature["properties"]["station_id"] != station_id:
                if not found_station_id:
                    continue
                else:
                    break
            if not found_station_id:
                geometry = feature["geometry"]
                found_station_id = True
            for name, native_name in to_sample.items():
                sample = {
                    "value": feature["properties"][native_name],
                    "time": dateutil.parser.parse(
                        feature["properties"]["time"]).isoformat(),
                    "level": settings.SURFACE_LEVEL
                }
                data_series[name].append(sample)
    return data_series, geometry

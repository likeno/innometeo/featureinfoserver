import logging
import logging.config
import datetime as dt
import math
import typing
from functools import partial
from pathlib import Path

import dateutil.parser
import falcon
import geojson
import numpy as np
import pandas as pd
import pyproj
import xarray as xr
from jinja2 import Environment, Template
from shapely import (
    geometry,
)

from . import observations
from . import settings
from . import skewt
from . import providers

LSASAF_PROVIDER = "lsasaf"
IPMA_PROVIDER = "ipma"
logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(__name__)


def modern_format(value, format_str):
    """A custom Jinja filter for formatting stuff.

    This is useful for allowing formats on datetime.datetime objects, as the
    default Jinja ``format`` filter does not work correctly.

    """

    try:
        result = format(value, format_str)
    except TypeError:
        result = value
    except ValueError:
        result = ""
    return result


def lonlat_to_epsg3857(lon: float, lat: float) -> typing.Tuple[float, float]:
    transformer = pyproj.Transformer.from_crs(4326, 3857, always_xy=True)
    return transformer.transform(lon, lat)


def get_relevant_settings(
        requested_layer_id: str
) -> typing.Tuple[
        str,
        typing.Dict,
        str,
        typing.Union[typing.List[typing.Dict], typing.Dict]
]:
    provider_name, layer = requested_layer_id.split(".")[:2]
    provider_settings = getattr(settings, provider_name.upper())
    layer_datasets = provider_settings["layers"][layer.lower()] or {}
    return provider_name, provider_settings, layer, layer_datasets


def retrieve_layer_info(
        area: str,
        point: geometry.Point,
        layer_datasets: typing.Union[typing.List[typing.Dict], typing.Dict],
        provider: typing.Dict,
        path_template: Template,
        reference_time: dt.datetime,
        times: typing.List[dt.datetime],
):
    result = []
    for dataset_params in layer_datasets:
        has_levels = dataset_params.get("levels")
        levels = provider["pressure_levels"] if has_levels else None
        dataset_info = retrieve_dataset_info(
            dataset_params["name"],
            path_template.render(
                data_root=settings.DATA_ROOT,
                reference_time=reference_time,
                area=area,
                run=reference_time.hour,
                pattern=dataset_params["pattern"]
            ),
            point,
            reference_time,
            times,
            levels=levels
        )
        result.extend(dataset_info)
    return result


def retrieve_dataset_info(
        variable_name: str,
        path: str,
        point: geometry.Point,
        reference_time: dt.datetime,
        times: typing.List[dt.datetime],
        levels: typing.Optional[typing.List[int]] = None
) -> typing.List[typing.Dict]:
    """Retrieve data from the input path at the specified coordinates"""
    logger.debug(f"locals: {locals()}")
    result = []
    with xr.open_dataset(path) as ds:
        for time in times:
            try:
                time_info = retrieve_time_info(
                    variable_name, point, time, reference_time, ds,
                    levels=levels
                )
            except RuntimeError as exc:
                logger.info(
                    f"Could not get info for time {time!r} - reason: {exc}")
            else:
                result.extend(time_info)
    return result


def retrieve_time_info(
        variable_name: str,
        point: geometry.Point,
        time: dt.datetime,
        reference_time: dt.datetime,
        dataset: xr.Dataset,
        levels: typing.Optional[typing.List[int]] = None
) -> typing.List:
    result = []
    step_delta = time - reference_time
    step = int(step_delta.days * 24 + step_delta.seconds / 3600)
    try:
        step_filter = dataset[variable_name].sel(
            step=pd.Timedelta(step, "hours"),
        )
    except KeyError as exc:
        raise RuntimeError(
            f"Could not find the appropriate time step - details: {exc}")
    # then proceed with selecting the exact spatial coordinates - here we
    # apply the `nearest` strategy, in order to get the closest value
    temporal_instant_array = step_filter.sel(
        x=point.x,
        y=point.y,
        method='nearest'
    )
    encoded_time = time.isoformat()
    if levels is None:
        value = float(temporal_instant_array)
        if not np.isnan(value):
            result.append({
                'value': value,
                'time': encoded_time,
                'level': settings.SURFACE_LEVEL
            })
    else:
        for level in levels:
            value = float(
                temporal_instant_array.sel(**{settings.LEVEL_NAME: level})
            )
            if not np.isnan(value):
                result.append({
                    'value': value,
                    'time': encoded_time,
                    'level': str(level),
                })
    return result


class JinjaMiddleware:
    """Injects a ``jinja.Environment`` instance into every request"""

    def process_resource(self, req, resp, resource, params):
        jinja_env = Environment()
        jinja_env.filters["format"] = modern_format
        req.context.jinja_env = jinja_env


def get_skewt_time(raw_time: dt.datetime) -> dt.datetime:
    # Get the time parameter from the request query string and process the time
    # value so that it matches the closest multiple of 3 hour
    return raw_time - dt.timedelta(hours=raw_time.hour % 3)


def open_grib_dataset(path, **kwargs):
    return xr.open_dataset(path, engine='cfgrib').sel(
        latitude=kwargs.get('latitude'),
        longitude=kwargs.get('longitude'),
        method='nearest'
    ).sel(step=pd.Timedelta(kwargs.get('step'), 'hours'))


def open_netcdf_dataset(path, **kwargs):
    with xr.open_dataset(path) as dataset:
        return dataset.sel(
            x=kwargs.get('x'), y=kwargs.get('y'), method='nearest'
        ).sel(step=pd.Timedelta(kwargs.get('step'), 'hours'))


def render_path_template(context, template, **kwargs):
    return context.jinja_env.from_string(template).render(
        data_root=kwargs.get('data_root'),
        reference_time=kwargs.get('reference_time'),
        area=kwargs.get('area'),
        run=kwargs.get('reference_time').hour,
        pattern=kwargs.get('pattern')
    )


class SkewDataResource:
    def on_get(self, request, response):
        reference_time = request.get_param_as_datetime(
            'reference_time', required=True)
        time = get_skewt_time(
            request.get_param_as_datetime("time", required=True))
        step = int((time - reference_time).seconds / 3600)
        # the original code for calculating `step` was:
        # step = time - reference_time
        # step = step.total_seconds() * 1000000000

        # Get the latitude and longitude from the request query string and process the
        # longitude value for the skewt data
        latitude = request.get_param_as_float('latitude', required=True)
        longitude = request.get_param_as_float('longitude', required=True)

        geometry = geojson.Point((latitude, longitude))
        feature = geojson.Feature(geometry=geometry)
        x, y = lonlat_to_epsg3857(longitude, latitude)

        data = {
            'reference_time': reference_time.strftime('%Y-%m-%dT%H:00:00Z'),
            'time': time.strftime('%Y-%m-%dT%H:00:00Z'),
            'levels': [],
        }

        ecmwf_data_pattern = settings.ECMWF["pattern"]
        ecmwf_data_layers = settings.ECMWF["layers"]
        ecmwf_atp_render_ctx = {
            'data_root': settings.DATA_ROOT,
            'reference_time': reference_time,
            'area': 'ATP',
            'run': reference_time.hour,
        }

        # CAPE
        cape_path = render_path_template(request.context, ecmwf_data_pattern,
            pattern=ecmwf_data_layers['cape'][0]['pattern'],
            **ecmwf_atp_render_ctx,
        )
        data['cape'] = float(open_netcdf_dataset(cape_path, x=x, y=y, step=step).cape)

        # CIN
        data['cin'] = None

        # Totalx
        data['totalx'] = None

        # Surface temperature
        surface_temperature_path = render_path_template(request.context, ecmwf_data_pattern,
            pattern=ecmwf_data_layers['temperature'][0]['pattern'],
            **ecmwf_atp_render_ctx,
        )
        data['2t'] = float(open_netcdf_dataset(surface_temperature_path, x=x, y=y, step=step).t2m)

        # Surface dewpoint
        surface_dewpoint_path = render_path_template(request.context, ecmwf_data_pattern,
            pattern=ecmwf_data_layers['dewpoint'][0]['pattern'],
            **ecmwf_atp_render_ctx,
        )
        data['2d'] = float(open_netcdf_dataset(surface_dewpoint_path, x=x, y=y, step=step).d2m)

        # Surface wind direction
        surface_wind_direction_path = render_path_template(request.context, ecmwf_data_pattern,
            pattern=ecmwf_data_layers['wind_direction'][0]['pattern'],
            **ecmwf_atp_render_ctx,
        )
        data['10winddir'] = float(open_netcdf_dataset(surface_wind_direction_path, x=x, y=y, step=step).direction)

        # Surface wind intensity
        surface_wind_intensity_path = render_path_template(request.context, ecmwf_data_pattern,
            pattern=ecmwf_data_layers['wind_intensity'][0]['pattern'],
            **ecmwf_atp_render_ctx,
        )
        data['10windint'] = float(open_netcdf_dataset(surface_wind_intensity_path, x=x, y=y, step=step).intensity)

        # Surface pressure
        surface_log_pressure_path = request.context.jinja_env.from_string(settings.ECMWF_ML["pattern"]).render(
            data_root=settings.DATA_ROOT,
            reference_time=reference_time,
            area='ATL',
            run=reference_time.hour,
            pattern=settings.ECMWF_ML['layers']['lnsp'][0]['pattern'])
        with xr.open_dataset(surface_log_pressure_path, engine='cfgrib') as surface_log_pressure:
            data['pressure'] = skewt.get_pressure_from_log_pressure(
                float(surface_log_pressure.sel(longitude=longitude, latitude=latitude, method='nearest').sel(step=pd.Timedelta(step, 'hours')).lnsp))


        level_temperature_path = request.context.jinja_env.from_string(settings.ECMWF_ML['pattern']).render(
            data_root=settings.DATA_ROOT,
            reference_time=reference_time,
            area='ATL',
            run=reference_time.hour,
            pattern=settings.ECMWF_ML['layers']['temperature'][0]['pattern']
        )
        level_specific_humidity_path = request.context.jinja_env.from_string(settings.ECMWF_ML['pattern']).render(
            data_root=settings.DATA_ROOT,
            reference_time=reference_time,
            area='ATL',
            run=reference_time.hour,
            pattern=settings.ECMWF_ML['layers']['specific_humidity'][0]['pattern']
        )
        level_wind_u_path = request.context.jinja_env.from_string(settings.ECMWF_ML['pattern']).render(
            data_root=settings.DATA_ROOT,
            reference_time=reference_time,
            area='ATL',
            run=reference_time.hour,
            pattern=settings.ECMWF_ML['layers']['wind_u'][0]['pattern']
        )
        level_wind_v_path = request.context.jinja_env.from_string(settings.ECMWF_ML['pattern']).render(
            data_root=settings.DATA_ROOT,
            reference_time=reference_time,
            area='ATL',
            run=reference_time.hour,
            pattern=settings.ECMWF_ML['layers']['wind_v'][0]['pattern']
        )

        level_data = {
            'temperature': open_grib_dataset(
                level_temperature_path, latitude=latitude, longitude=longitude, step=step),
            'specific_humidity': open_grib_dataset(
                level_specific_humidity_path, latitude=latitude, longitude=longitude, step=step),
            'wind_u': open_grib_dataset(
                level_wind_u_path, latitude=latitude, longitude=longitude, step=step),
            'wind_v': open_grib_dataset(
                level_wind_v_path, latitude=latitude, longitude=longitude, step=step),
        }

        for level in level_data['temperature'].hybrid:
            level_output_data = {}
            level_index = int(level)

            level_output_data['level'] = level_index
            level_output_data['press'] = skewt.get_level_pressure(level_index, data['pressure'])
            level_output_data['temp'] = float(
                level_data['temperature'].sel(hybrid=level_index).t)
            level_output_data['humidity'] = float(
                level_data['specific_humidity'].sel(hybrid=level_index).q)
            level_output_data['dwpt'] = skewt.specific_humidity_to_dewpoint_temperature(
                float(level_data['specific_humidity'].sel(hybrid=level_index).q),
                skewt.get_level_pressure(level_index, data['pressure']))

            u = float(level_data['wind_u'].sel(hybrid=level_index).u)
            v = float(level_data['wind_v'].sel(hybrid=level_index).v)
            level_output_data['wspd'] = math.sqrt(u*u + v*v)
            level_output_data['wdir'] = math.atan2(v, u) * (180.0 / math.pi)
            level_output_data['hght'] = skewt.get_level_geometric_altitude(level_index)

            data['levels'].append(level_output_data)


        feature['properties'] = data
        response.body = geojson.dumps(feature)


class FeatureInfoResource:

    def on_get(self, request, response):
        lat = request.get_param_as_float('latitude', required=True)
        lon = request.get_param_as_float('longitude', required=True)
        requested_layers = request.get_param_as_list('layer', required=True)
        reference_time = request.get_param_as_datetime(
            "reference_time",
            required=True,
            format_string="%Y-%m-%dT%H:%M:%S%z"
        )
        times_parser = partial(
            dateutil.parser.parse, default=dt.datetime.now(dt.timezone.utc))
        times = request.get_param_as_list(
            "time",
            required=True,
            transform=times_parser
        )
        logger.debug(f"reference_time: {reference_time}")
        logger.debug(f"times: {times}")
        logger.debug(f"requested_layers: {requested_layers}")
        x, y = lonlat_to_epsg3857(lon, lat)
        logger.debug(f"lon: {lon}, lat: {lat}, x: {x}, y: {y}")
        point_to_sample = geometry.Point(x, y)
        feature = geojson.Feature(geometry=geojson.Point((lat, lon)))
        for layer_id in requested_layers:
            try:
                relevant = get_relevant_settings(layer_id)
                prov_name, prov_settings, layer_name, layer_datasets = relevant
                logger.debug(f"provider: {prov_name}")
                logger.debug(f"layer_name: {layer_name}")
                logger.debug(f"layer_datasets: {layer_datasets}")
            except (IndexError, AttributeError, KeyError):
                logger.debug(f"layer {layer_id!r} is invalid, skipping...")
                continue
            path_template = request.context.jinja_env.from_string(
                prov_settings["pattern"])
            for area_name, geom in prov_settings["areas"].items():
                if geom.contains(point_to_sample):
                    logger.debug(
                        f"layer {layer_id!r} intersects {point_to_sample} "
                        f"in area {area_name}"
                    )
                    if prov_name in (LSASAF_PROVIDER, IPMA_PROVIDER):
                        layer_info = providers.retrieve_layer_info(
                            layer_name, prov_name, point_to_sample,
                            layer_datasets, path_template, reference_time
                        )
                    else:
                        layer_info = retrieve_layer_info(
                            area_name, point_to_sample, layer_datasets,
                            prov_settings, path_template, reference_time, times
                        )
                    feature["properties"][layer_id] = layer_info
                    break  # no need to try remaining areas
                else:
                    logger.debug(
                        f"layer {layer_id!r} does not "
                        f"intersect {point_to_sample} in area {area_name}, "
                        f"skipping..."
                    )
            else:
                logger.debug(
                    f"layer {layer_id} does not intersect "
                    f"{point_to_sample} in any areas, skipping..."
                )
        response.body = geojson.dumps(feature)


class HandleCORS:
    def process_request(self, request, response):
        response.set_header('Access-Control-Allow-Origin', '*')


class ObservationResource:

    def on_get_list(
            self,
            request: falcon.Request,
            response: falcon.Response,
            dataset_name: str,
    ):
        validate_dataset_name(dataset_name)
        try:
            instant = request.get_param_as_datetime(
                "time").replace(tzinfo=dt.timezone.utc)
        except AttributeError:
            instant = None
        raw_bbox = request.get_param("bbox")
        logger.debug(f"raw_bbox: {raw_bbox!r}")
        if raw_bbox is None:
            bbox = raw_bbox
        else:
            try:
                bbox = parse_bbox(request.get_param("bbox"))
            except ValueError:
                raise falcon.HTTPInvalidParam(
                    "Invalid bounding box - supply a value in the form "
                    "minx,maxx,miny,maxy", "bbox"
                )
        path = get_observations_path(request.context.jinja_env)
        features = observations.extract_observation_data(
            path, dataset_name, instant, bbox)
        collection = geojson.FeatureCollection(features)
        response.body = geojson.dumps(collection)


class StationResource:

    def on_get_list(
            self,
            request: falcon.Request,
            response: falcon.Response,
            station_id: int
    ):
        raw_datasets = request.get_param_as_list("dataset")
        if raw_datasets is not None:
            datasets = [validate_dataset_name(d) for d in raw_datasets]
        else:
            datasets = None
        logger.debug(f"datasets: {datasets}")
        path = get_observations_path(request.context.jinja_env)
        data_series, geometry = observations.extract_station_data(
            path,
            station_id,
            datasets=datasets
        )
        feature = geojson.Feature(geometry=geometry, properties=data_series)
        response.body = geojson.dumps(feature)


def get_observations_path(jinja_env: Environment):
    path_template = jinja_env.from_string(settings.OBSERVATIONS["pattern"])
    path_context = {
        "data_root": settings.DATA_ROOT,
    }
    return Path(path_template.render(path_context))


def validate_dataset_name(name: str):
    if name not in settings.OBSERVATIONS["datasets"].keys():
        raise falcon.HTTPNotFound(
            title="Not Found", description="Not Found")
    return name


def parse_bbox(bbox: str) -> geometry.Polygon:
    min_x, max_x, min_y, max_y = [float(i) for i in bbox.split(",")]
    return geometry.Polygon((
        (min_x, min_y),
        (max_x, min_y),
        (max_x, max_y),
        (min_x, max_y),
        (min_x, min_y),
    ))


api = falcon.API(
    middleware=[
        JinjaMiddleware(),
        HandleCORS(),
    ]
)
api.add_route('/query', FeatureInfoResource())
api.add_route('/skew', SkewDataResource())
api.add_route(
    '/observations/{dataset_name}', ObservationResource(), suffix="list")
api.add_route('/stations/{station_id:int}', StationResource(), suffix="list")
